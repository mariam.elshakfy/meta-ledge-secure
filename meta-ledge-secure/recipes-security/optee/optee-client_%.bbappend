FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
	file://create-tee-supplicant-env \
        file://optee-udev.rules \
	"

EXTRA_OECMAKE:append = " \
    -DRPMB_EMU=0 \
"

do_install:append() {
	install -D -p -m0755 ${WORKDIR}/create-tee-supplicant-env ${D}${sbindir}/create-tee-supplicant-env
        sed -i 's|^ExecStart=.*$|Type=forking\nEnvironmentFile=-@localstatedir@/run/tee-supplicant.env\nExecStartPre=-/sbin/modprobe -r tpm_ftpm_tee\nExecStartPre=@sbindir@/create-tee-supplicant-env @localstatedir@/run/tee-supplicant.env\nExecStart=@sbindir@/tee-supplicant $RPMB_CID $OPTARGS -d\nExecStartPost=-/bin/sh -c "while [ ! $(pgrep tee-supplicant) ]; do sleep 0.1; done; /sbin/modprobe tpm_ftpm_tee"\nExecStop=-/sbin/modprobe -r tpm_ftpm_tee|' ${D}${systemd_system_unitdir}/tee-supplicant@.service
	sed -i -e s:@sbindir@:${sbindir}:g \
	       -e s:@localstatedir@:${localstatedir}:g ${D}${systemd_system_unitdir}/tee-supplicant@.service
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/optee-udev.rules ${D}${sysconfdir}/udev/rules.d/optee.rules
	# blacklist the ftp module since we load it manually and has to be inserted after the
	# tee-supplicant.  This is fixed in newer kernel so we might remove it when we upgrade
	install -d ${D}/etc/modprobe.d/
	echo 'blacklist tpm_ftpm_tee' >> ${D}/etc/modprobe.d/blacklist-ftpm.conf
}

inherit useradd
USERADD_PACKAGES = "${PN}"
GROUPADD_PARAM:${PN} = "--system teeclnt"
