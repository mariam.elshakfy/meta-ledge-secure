inherit kernel siteinfo

DEPENDS += "coreutils-native"

# strip kernel modules before signing to reduce module size
EXTRA_OEMAKE += "INSTALL_MOD_STRIP=1"

FILESEXTRAPATHS:prepend := "${THISDIR}:${THISDIR}/linux-yocto:"

SRC_URI += "file://ledge-kmeta;type=kmeta;name=ledge-kmeta;destsuffix=ledge-kmeta"
SRC_URI:append:qemuarm =   " file://ledgeqemuarm-kmeta;type=kmeta;name=ledgeqemuarm-kmeta;destsuffix=ledgeqemuarm-kmeta"
SRC_URI:append:aarch64 = " file://ledgearm64-kmeta;type=kmeta;name=ledgearm64-kmeta;destsuffix=ledgearm64-kmeta"

# major feature config snippets
KERNEL_FEATURES += "cfg/efi-ext.scc"
KERNEL_FEATURES += "cfg/net/ipv6.scc"
KERNEL_FEATURES += "cfg/virtio.scc"
KERNEL_FEATURES += "features/bluetooth/bluetooth.scc"
KERNEL_FEATURES += "features/media/media-all.scc"
KERNEL_FEATURES += "features/usb/serial-all.scc"
KERNEL_FEATURES += "features/wifi/wifi-all.scc"
KERNEL_FEATURES += "features/zram/zram.scc"
KERNEL_FEATURES += "fragment-1-selinux.cfg"
KERNEL_FEATURES += "fragment-2-secure.cfg"
KERNEL_FEATURES += "fragment-3-virtio.cfg"
KERNEL_FEATURES += "fragment-acpi.cfg"
KERNEL_FEATURES += "fragment-blk.cfg"
KERNEL_FEATURES += "fragment-composefs.cfg"
KERNEL_FEATURES += "fragment-debug.cfg"
# to identify different boards via /sys/class/dmi/id/board_vendor
KERNEL_FEATURES += "fragment-dmi.cfg"
KERNEL_FEATURES += "fragment-drm.cfg"
KERNEL_FEATURES += "fragment-powersave.cfg"
KERNEL_FEATURES += "fragment-seccomp.cfg"
KERNEL_FEATURES += "fragment-security.cfg"
KERNEL_FEATURES += "fragment-tee.cfg"

# board/HW specific config snippets
KERNEL_FEATURES:append:aarch64 = " fragment-ava.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-aws.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-imx.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-rockchip-media.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-rockchip.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-zynqmp.cfg"
KERNEL_FEATURES:append:qemuarm = " watchdog.cfg"

# use "defconfig" from kernel source arch/arm64/configs, then add our config fragments on top
KBUILD_DEFCONFIG:aarch64 = "defconfig"
# from kernel source arch/arm/configs to follow upstream defaults with new kernel versions
KBUILD_DEFCONFIG:arm = "multi_v7_defconfig"
# apply upstream defaults after defconfig and config fragments, to support new kernel
# features and defaults automatically
KCONFIG_MODE = "alldefconfig"

SRC_URI += " \
    file://0001-rk3399-rock-pi-4.dtsi-enable-imx219-isp.patch \
    file://0002-npu-ethosu-Add-Arm-ethos-u-driver.patch \
    file://0003-npu-ethosu-Add-Rpmsg-support-based-on-i.MX-Rpmsg-imp.patch \
    file://0004-ethosu-add-inference-type-to-support-A-core-tflite.patch \
    file://0005-ethosu-Npu-only-supports-4-pmu-events.patch \
    file://0006-ethosu-Add-remoteproc-support-in-the-NPU-driver.patch \
    file://0007-ethosu-Add-suspend-resume-power-management.patch \
    file://0008-ethosu-Add-switch-for-NPU-power-management.patch \
    file://0009-ethosu-Fix-kernel-call-trace-before-rpmsg-dev-probe.patch \
    file://0010-ethosu-Add-message-type-ETHOSU_CORE_MSG_POWER_RSP.patch \
    file://0011-ethosu-Check-size-of-buffer-before-allocating.patch \
    file://0012-ethosu-Use-ids-for-identifying-messages-sent-to-Etho.patch \
    file://0013-ethosu-Update-uapi-definition-for-capability-request.patch \
    file://0014-ethosu-Add-core-message-about-network-info.patch \
    file://0015-ethosu-Add-support-for-inference-cancellation.patch \
    file://0016-ethosu-Add-input-and-output-types-shapes-offset-in-n.patch \
    file://0017-ethosu-Fix-memcpy-overflow-issue.patch \
    file://0018-ethosu-Fix-implicit-conversion-warning.patch \
    file://0019-LF-7941-ethosu-fix-potential-out-of-bounds-issue.patch \
    file://0020-LF-8029-ethosu-Update-the-interface-for-printing-hex.patch \
    file://0021-AIR-8353-ethosu-add-rwlock-when-alloc-and-remove-msg.patch \
    file://0022-rpmsg-imx-add-the-initial-imx-rpmsg-support.patch \
    file://0023-rpmsg-imx-extend-the-rpmsg-support-for-imx8qm-and-so.patch \
    file://0024-rpmsg-imx-bug-fix-and-clean-up-the-codes.patch \
    file://0025-rpmsg-imx_rpmsg-add-partition-reset-notify.patch \
    file://0026-rpmsg-imx-enable-the-tx_block-mechanism-in-the-flow.patch \
    file://0027-rpmsg-imx-remove-use-of-ioremap_nocache.patch \
    file://0028-LF-44-rpmsg-imx-add-the-rpmsg-tty-demo.patch \
    file://0029-LF-2108-rpmsg-imx-fix-the-pointer-conversion-loses-b.patch \
    file://0030-MLK-25649-7-rpmsg-imx-Add-support-for-identifying-SC.patch \
    file://0031-imx_rpmsg_tty-Fix-build.patch \
    file://0032-imx_rpmsg-include-macro-definition.patch \
    file://0033-rpmsg-imx_rpmsg-Change-the-notifying-to-timeout-mode.patch \
    file://0034-remoteproc-imx_rproc-use-imx-specific-hook-for-find_.patch \
    file://0035-LF-6794-remoteproc-imx_rproc-initialize-workqueue-ea.patch \
    file://0036-remoteproc-imx_rproc-make-clk-optional.patch \
    file://0037-imx_rpmsg_tty-fix-types-in-rpmsgtty_write.patch \
"

do_compile:append:aarch64() {
    oe_runmake -C ${B} dtbs
}

do_compile:append:qemuarm() {
    oe_runmake -C ${B} dtbs
}

do_install:append:aarch64() {
    oe_runmake -C ${B} DEPMOD=echo INSTALL_DTBS_PATH=${D}/boot/dtb dtbs_install
}

do_install:append:qemuarm() {
    oe_runmake -C ${B} DEPMOD=echo INSTALL_DTBS_PATH=${D}/boot/dtb dtbs_install
}

do_install:append() {
    if [ ! -d ${D}/boot/dtb ]; then
        # force the creation of dtb directory on boot to have
        install -d ${D}/boot/dtb
        echo "Empty content on case there is no devicetree" > ${D}/boot/dtb/.emtpy
    fi

    #rename device tree
    for dtb in ${DTB_RENAMING}
    do
        dtb_orignal=$(echo $dtb | cut -d':' -f 1 )
        dtb_renamed=$(echo $dtb | cut -d':' -f 2 )

        if [ -f ${D}/boot/$dtb_orignal ]; then
            cd ${D}/boot/
            ln -s $dtb_orignal $dtb_renamed
           cd -
        fi
        if [ -f ${D}/boot/dtb/$dtb_orignal ]; then
            cd ${D}/boot/dtb/
            ln -s $dtb_orignal $dtb_renamed
            cd -
        fi
    done
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}/dtb
    if [ -d ${WORKDIR}/package/boot/dtb ];then
        cp -rf ${WORKDIR}/package/boot/dtb ${DEPLOYDIR}/
    fi
}

FILES:${KERNEL_PACKAGE_NAME}-base += "/${KERNEL_IMAGEDEST}/dtb"
FILES:${KERNEL_PACKAGE_NAME}-base += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/modules.builtin.modinfo "
FILES:${KERNEL_PACKAGE_NAME}-base += "${base_libdir}/modprobe.d"
