FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

DESCRIPTION = "Small ramdisk image for secure boot and TPM sealed and encrypted rootfs"
PV="1.1"

PACKAGE_INSTALL = " \
    ${@bb.utils.contains("MACHINE_FEATURES", "optee", "optee-client", "", d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'udev', 'eudev', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'sota', "ostree-switchroot", "", d)} \
    ${ROOTFS_BOOTSTRAP_INSTALL} \
    ${VIRTUAL-RUNTIME_base-utils} \
    base-passwd \
    coreutils \
    cryptfs-tpm2 \
    dbus \
    e2fsprogs-mke2fs \
    efivar \
    kernel-module-dm-crypt \
    kernel-module-dm-mod \
    kernel-module-tpm-ftpm-tee \
    kernel-module-tpm-tis \
    kernel-module-tpm-tis-core \
    kmod \
    ledge-init \
    lvm2-udevrules \
    mmc-utils \
    os-release-initrd \
    rng-tools \
    smartmontools \
    tpm2-abrmd \
    tpm2-tss \
    tpm2-tss-engine \
    util-linux-blkid \
    util-linux-mount \
    util-linux-umount \
"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""

export IMAGE_NAME = "ledge-initramfs"
IMAGE_LINGUAS = ""

LICENSE = "MIT"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
inherit core-image

INITRAMFS_MAXSIZE ?= "262144"
IMAGE_ROOTFS_SIZE = "32768"
IMAGE_ROOTFS_EXTRA_SPACE = "4096"

# Disable installation of kernel and modules via packagegroup-core-boot
NO_RECOMMENDATIONS = "1"
